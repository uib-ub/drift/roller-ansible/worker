Worker
=========

Creates a system account and adds a private key if applies.
Is used for setting up access by deploy keys for servers using git repos, and to set up a non-root user to add cron-jobs to.

* Creates a system user with a name (and a home folder)
* (Optional) Adds a private key for user.
* Adds known hosts (uib git server) to known hosts.

Role is repeatable to add multiple system users.

Requirements
------------


Role Variables
--------------

`worker_name` creates a user as a given name. 
`worker_keyfile` is copied into /home/{{worker_name}}/.ssh. 

Use ansible-vault for keyfile.

Dependencies
------------

No dependencies.

Example Playbook
----------------

```
    - hosts: servers
      roles:
         - { role: worker, worker_name: "protege", worker_keyfile: private_key,   worker_git_email: "ubbdst@gmail.com"
 }
         - { role: worker, worker_name: "cron_worker" ...}
    - task:
        crontab:
          user: "cron_worker" 
          ...
```
License
-------

BSD

Author Information
------------------


